import * as React from 'react'
import { View, ScrollView, ViewStyle } from 'react-native'
import * as _ from 'lodash'

// Interfaces
import { IColStates } from './../index.d'

// Base
import Base from './Base'

export default class Col extends Base {
  public ViewComponent() {
    const { containerStyle, direction, size, style } = this._processProps()
    let _style = _.clone(style)

    const contentContainerStyle: ViewStyle = {
      flexDirection: 'column',
      flexGrow: 1,
    }
    const defaultContainerStyle: ViewStyle = {
      flex: size ? size : style && this._filterStyle('width') ? 0 : 1,
      flexDirection: 'column',
    }

    const defaultStyle = {
      padding: this.isPadding() ? 16 : 0,
    }

    if (this._filterStyle('justifyContent')) {
      contentContainerStyle.justifyContent = this._filterStyle('justifyContent')
      _style = this._deleteStyle(_style, 'justifyContent')
    }

    if (this._filterStyle('alignItems')) {
      contentContainerStyle.alignItems = this._filterStyle('alignItems')
      _style = this._deleteStyle(_style, 'alignItems')
    }

    if (direction) {
      defaultContainerStyle.flexDirection = direction
      contentContainerStyle.flexDirection = direction
    }

    return (
      <View style={ [defaultContainerStyle, containerStyle] }>
        <ScrollView style={ [defaultStyle, _style] } contentContainerStyle={ contentContainerStyle }>
          { this.getChildren() }
        </ScrollView>
      </View>
    )
  }

  public _processProps(): IColStates {
    const { containerStyle, direction, noPadding, noScroll, onPress, options, padding, scroll, size, style } = this.props

    const props: IColStates = {
      containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
      direction: (options && options.direction) || (direction || undefined),
      noPadding: (options && options.noPadding) || (noPadding || true),
      noScroll: (options && options.noScroll) || (noScroll || true),
      onPress: (options && options.onPress) || (onPress || undefined),
      padding: (options && options.padding) || (padding || false),
      scroll: (options && options.scroll) || (scroll || false),
      size: (options && options.size) || (size || undefined),
      style: (options && options.style) || (style || undefined),
    }

    return props
  }
}
