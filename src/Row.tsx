import * as React from 'react'
import { View, TouchableOpacity, ScrollView, ViewStyle } from 'react-native'
import * as _ from 'lodash'

// Interfaces
import { IRowProps, IRowStates } from './../index.d'

// Base
import Base from './Base'

export default class Row extends Base {
  public ViewComponent() {
    const { containerStyle, direction, size, style } = this._processProps()
    let _style = _.clone(style)

    const contentContainerStyle: ViewStyle = {
      flexDirection: 'row',
      flexGrow: 1,
    }
    const defaultContainerStyle: ViewStyle = {
      flex: size ? size : style && this._filterStyle('height') ? 0 : 1,
      flexDirection: 'row',
    }

    const defaultStyle = {
      padding: this.isPadding() ? 16 : 0,
    }

    if (this._filterStyle('justifyContent')) {
      contentContainerStyle.justifyContent = this._filterStyle('justifyContent')
      _style = this._deleteStyle(_style, 'justifyContent')
    }

    if (this._filterStyle('alignItems')) {
      contentContainerStyle.alignItems = this._filterStyle('alignItems')
      _style = this._deleteStyle(_style, 'alignItems')
    }

    if (direction) {
      defaultContainerStyle.flexDirection = direction
      contentContainerStyle.flexDirection = direction
    }

    return (
      <View style={ [defaultContainerStyle, containerStyle] }>
        <ScrollView style={ [defaultStyle, _style] } contentContainerStyle={ contentContainerStyle }>
          { this.getChildren() }
        </ScrollView>
      </View>
    )
  }

  public _processProps(): IRowStates {
    const { containerStyle, noPadding, noScroll, onPress, options, padding, scroll, size, style } = this.props

    const props: IRowStates = {
      containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
      noPadding: (options && options.noPadding) || (noPadding || true),
      noScroll: (options && options.noScroll) || (noScroll || true),
      onPress: (options && options.onPress) || (onPress || undefined),
      padding: (options && options.padding) || (padding || false),
      scroll: (options && options.scroll) || (scroll || false),
      size: (options && options.size) || (size || undefined),
      style: (options && options.style) || (style || undefined),
    }

    return props
  }
}
