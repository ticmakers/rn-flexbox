import * as React from 'react'
import { View, TouchableOpacity, ScrollView, ViewStyle } from 'react-native'
import * as _ from 'lodash'

// Interfaces
import { IBaseProps, IBaseStates, TypeStyle } from './../index.d'

export default class Base extends React.Component<IBaseProps, IBaseStates> {
  constructor(props: IBaseProps) {
    super(props)
    this.state = this._processProps()
  }

  public render() {
    const { onPress } = this._processProps()

    if (onPress) {
      return(
        <TouchableOpacity onPress={ () => this._onPress() }>
          { this.ViewComponent }
        </TouchableOpacity>
      )
    }

    return this.ViewComponent()
  }

  public ViewComponent() {
    const { containerStyle, direction, size, style } = this._processProps()
    let _style = _.clone(style)

    const contentContainerStyle: ViewStyle = {
      flexGrow: 1,
    }
    const defaultContainerStyle: ViewStyle = {
      flex: size ? size : style && this._filterStyle('height') ? 0 : 1,
    }

    const defaultStyle = {
      padding: this.isPadding() ? 16 : 0,
    }

    if (this._filterStyle('justifyContent')) {
      contentContainerStyle.justifyContent = this._filterStyle('justifyContent')
      _style = this._deleteStyle(_style, 'justifyContent')
    }

    if (this._filterStyle('alignItems')) {
      contentContainerStyle.alignItems = this._filterStyle('alignItems')
      _style = this._deleteStyle(_style, 'alignItems')
    }

    if (direction) {
      defaultContainerStyle.flexDirection = direction
      contentContainerStyle.flexDirection = direction
    }

    return (
      <View style={ [defaultContainerStyle, containerStyle] }>
        <ScrollView style={ [defaultStyle, _style] } contentContainerStyle={ contentContainerStyle }>
          { this.getChildren() }
        </ScrollView>
      </View>
    )
  }

  public isPadding() {
    const { noPadding, padding } = this._processProps()
    return !noPadding || (padding && !noPadding)
  }

  public isScroll() {
    const { noScroll, scroll } = this._processProps()
    return !noScroll || (scroll && !noScroll)
  }

  public getChildren() {
    return this.props.children
  }

  public _onPress() {
    const { onPress } = this._processProps()

    if (onPress) {
      return onPress()
    }
  }

  public _processProps(): IBaseStates {
    const { containerStyle, direction, noPadding, noScroll, onPress, options, padding, scroll, size, style } = this.props

    const props: IBaseStates = {
      containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
      direction: (options && options.direction) || (direction || undefined),
      noPadding: (options && options.noPadding) || (noPadding || true),
      noScroll: (options && options.noScroll) || (noScroll || true),
      onPress: (options && options.onPress) || (onPress || undefined),
      padding: (options && options.padding) || (padding || false),
      scroll: (options && options.scroll) || (scroll || false),
      size: (options && options.size) || (size || undefined),
      style: (options && options.style) || (style || undefined),
    }

    return props
  }

  public _filterStyle(prop: string) {
    const { style } = this.props
    let res

    if (style && Array.isArray(style)) {
      for (const _style of style) {
        if (_style  && typeof _style[prop] !== 'undefined') {
          res = _style[prop]
        }
      }
    } else if (style) {
      res = style[prop]
    }

    return res
  }

  public _deleteStyle(style: TypeStyle, prop: string) {
    let _style: TypeStyle = {}

    if (style && Array.isArray(style)) {
      _style = []

      style.forEach((s) => {
        const _s = {}

        Object.keys(s as {}).forEach((k: string) => {
          if (k !== prop) {
            _s[k] = s && s[k]
          }
        })

        _style && (_style as ViewStyle[]).push(_s)
      })
    } else if (style) {
      _style = {}

      Object.keys(style).forEach((k: string) => {
        if (k !== prop && _style) {
          _style[k] = style[k]
        }
      })
    }

    return _style
  }
}
