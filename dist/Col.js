"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const _ = require("lodash");
const Base_1 = require("./Base");
class Col extends Base_1.default {
    ViewComponent() {
        const { containerStyle, direction, size, style } = this._processProps();
        let _style = _.clone(style);
        const contentContainerStyle = {
            flexDirection: 'column',
            flexGrow: 1,
        };
        const defaultContainerStyle = {
            flex: size ? size : style && this._filterStyle('width') ? 0 : 1,
            flexDirection: 'column',
        };
        const defaultStyle = {
            padding: this.isPadding() ? 16 : 0,
        };
        if (this._filterStyle('justifyContent')) {
            contentContainerStyle.justifyContent = this._filterStyle('justifyContent');
            _style = this._deleteStyle(_style, 'justifyContent');
        }
        if (this._filterStyle('alignItems')) {
            contentContainerStyle.alignItems = this._filterStyle('alignItems');
            _style = this._deleteStyle(_style, 'alignItems');
        }
        if (direction) {
            defaultContainerStyle.flexDirection = direction;
            contentContainerStyle.flexDirection = direction;
        }
        return (React.createElement(react_native_1.View, { style: [defaultContainerStyle, containerStyle] },
            React.createElement(react_native_1.ScrollView, { style: [defaultStyle, _style], contentContainerStyle: contentContainerStyle }, this.getChildren())));
    }
    _processProps() {
        const { containerStyle, direction, noPadding, noScroll, onPress, options, padding, scroll, size, style } = this.props;
        const props = {
            containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
            direction: (options && options.direction) || (direction || undefined),
            noPadding: (options && options.noPadding) || (noPadding || true),
            noScroll: (options && options.noScroll) || (noScroll || true),
            onPress: (options && options.onPress) || (onPress || undefined),
            padding: (options && options.padding) || (padding || false),
            scroll: (options && options.scroll) || (scroll || false),
            size: (options && options.size) || (size || undefined),
            style: (options && options.style) || (style || undefined),
        };
        return props;
    }
}
exports.default = Col;
//# sourceMappingURL=Col.js.map