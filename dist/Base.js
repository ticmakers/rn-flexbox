"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_native_1 = require("react-native");
const _ = require("lodash");
class Base extends React.Component {
    constructor(props) {
        super(props);
        this.state = this._processProps();
    }
    render() {
        const { onPress } = this._processProps();
        if (onPress) {
            return (React.createElement(react_native_1.TouchableOpacity, { onPress: () => this._onPress() }, this.ViewComponent));
        }
        return this.ViewComponent();
    }
    ViewComponent() {
        const { containerStyle, direction, size, style } = this._processProps();
        let _style = _.clone(style);
        const contentContainerStyle = {
            flexGrow: 1,
        };
        const defaultContainerStyle = {
            flex: size ? size : style && this._filterStyle('height') ? 0 : 1,
        };
        const defaultStyle = {
            padding: this.isPadding() ? 16 : 0,
        };
        if (this._filterStyle('justifyContent')) {
            contentContainerStyle.justifyContent = this._filterStyle('justifyContent');
            _style = this._deleteStyle(_style, 'justifyContent');
        }
        if (this._filterStyle('alignItems')) {
            contentContainerStyle.alignItems = this._filterStyle('alignItems');
            _style = this._deleteStyle(_style, 'alignItems');
        }
        if (direction) {
            defaultContainerStyle.flexDirection = direction;
            contentContainerStyle.flexDirection = direction;
        }
        return (React.createElement(react_native_1.View, { style: [defaultContainerStyle, containerStyle] },
            React.createElement(react_native_1.ScrollView, { style: [defaultStyle, _style], contentContainerStyle: contentContainerStyle }, this.getChildren())));
    }
    isPadding() {
        const { noPadding, padding } = this._processProps();
        return !noPadding || (padding && !noPadding);
    }
    isScroll() {
        const { noScroll, scroll } = this._processProps();
        return !noScroll || (scroll && !noScroll);
    }
    getChildren() {
        return this.props.children;
    }
    _onPress() {
        const { onPress } = this._processProps();
        if (onPress) {
            return onPress();
        }
    }
    _processProps() {
        const { containerStyle, direction, noPadding, noScroll, onPress, options, padding, scroll, size, style } = this.props;
        const props = {
            containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
            direction: (options && options.direction) || (direction || undefined),
            noPadding: (options && options.noPadding) || (noPadding || true),
            noScroll: (options && options.noScroll) || (noScroll || true),
            onPress: (options && options.onPress) || (onPress || undefined),
            padding: (options && options.padding) || (padding || false),
            scroll: (options && options.scroll) || (scroll || false),
            size: (options && options.size) || (size || undefined),
            style: (options && options.style) || (style || undefined),
        };
        return props;
    }
    _filterStyle(prop) {
        const { style } = this.props;
        let res;
        if (style && Array.isArray(style)) {
            for (const _style of style) {
                if (_style && typeof _style[prop] !== 'undefined') {
                    res = _style[prop];
                }
            }
        }
        else if (style) {
            res = style[prop];
        }
        return res;
    }
    _deleteStyle(style, prop) {
        let _style = {};
        if (style && Array.isArray(style)) {
            _style = [];
            style.forEach((s) => {
                const _s = {};
                Object.keys(s).forEach((k) => {
                    if (k !== prop) {
                        _s[k] = s && s[k];
                    }
                });
                _style && _style.push(_s);
            });
        }
        else if (style) {
            _style = {};
            Object.keys(style).forEach((k) => {
                if (k !== prop && _style) {
                    _style[k] = style[k];
                }
            });
        }
        return _style;
    }
}
exports.default = Base;
//# sourceMappingURL=Base.js.map