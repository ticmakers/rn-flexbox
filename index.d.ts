import * as React from 'react'
import {
  RecursiveArray,
  RegisteredStyle,
  ScrollViewProps,
  StyleProp,
  TouchableOpacityProps,
  ViewProps,
  ViewStyle,
} from 'react-native'

/**
 * Type to define the prop style of the Login component
 */
export type TypeStyle = false | ViewStyle | RegisteredStyle<ViewStyle> | RecursiveArray<false | ViewStyle | RegisteredStyle<ViewStyle> | null | undefined> | StyleProp<ViewStyle>[] | null | undefined

/**
 * Interface to define the states base of a component
 * @export
 * @interface IBaseStates
 * @extends {ViewProps}
 * @extends {ScrollViewProps}
 * @extends {TouchableOpacityProps}
 */
export interface IBaseStates extends ViewProps, ScrollViewProps, TouchableOpacityProps {
  /**
   * Apply styles to the container of the component
   */
  containerStyle?: TypeStyle

  /**
   * Define the flex direction (row | column)
   */
  direction?: 'row' | 'column'

  /**
   * Set true to remove the padding of the component
   */
  noPadding?: boolean

  /**
   * Set true to disable the scroll
   */
  noScroll?: boolean

  /**
   * Method that fire when the component is pressed
   */
  onPress?: () => any

  /**
   * Set true to add padding to the component
   */
  padding?: boolean

  /**
   * Set true to enable the scroll
   */
  scroll?: boolean

  /**
   * Define the size of the component
   */
  size?: number

  /**
   * Define a custom style to the component
   */
  style?: TypeStyle
}

/**
 * Interface to define the props of the Base component
 * @export
 * @interface IBaseProps
 * @extends {IBaseStates}
 */
export interface IBaseProps extends IBaseStates {
  /**
  * Prop for group all the props of the Input component
  */
 options?: IBaseStates
}

/**
 * Interface to define the states of the Grid component
 * @export
 * @interface IGridStates
 */
export interface IGridStates extends IBaseStates  {
}

/**
 * Interface to define the props of the Grid component
 * @export
 * @interface IGridProps
 * @extends {IGridStates}
 */
export interface IGridProps extends IGridStates {
   /**
   * Prop for group all the props of the Input component
   */
  options?: IGridStates
}

/**
 * Interface to define the states of the Row component
 * @export
 * @interface IRowStates
 */
export interface IRowStates extends IBaseStates {
}

/**
 * Interface to define the props of the Row component
 * @export
 * @interface IRowProps
 * @extends {IRowStates}
 */
export interface IRowProps extends IRowStates {
   /**
   * Prop for group all the props of the Input component
   */
  options?: IRowStates
}

/**
 * Interface to define the states of the Col component
 * @export
 * @interface IColStates
 */
export interface IColStates extends IBaseStates {
}

/**
 * Interface to define the props of the Col component
 * @export
 * @interface IColProps
 * @extends {IColStates}
 */
export interface IColProps extends IColStates {
   /**
   * Prop for group all the props of the Input component
   */
  options?: IColStates
}


/**
 * Declaration for Grid component
 * @class Grid
 * @extends {React.Component<IGridProps, IGridStates>}
 */
declare class Grid extends React.Component<IGridProps, IGridStates> {
}

/**
 * Declaration for Row component
 * @class Row
 * @extends {React.Component<IRowProps, IRowStates>}
 */
declare class Row extends React.Component<IRowProps, IRowStates> {
}

/**
 * Declaration for Col component
 * @class Col
 * @extends {React.Component<IColProps, IColStates>}
 */
declare class Col extends React.Component<IColProps, IColStates> {
}

/**
 * Declaration for Flexbox module
 */
declare module '@ticmakers-react-native/flexbox'

/**
 * Export default
 */
export { Col, Grid, Row }
