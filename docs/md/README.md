
TIC Makers - React Native Flexbox
=================================

React native component for flexbox.

Powered by [TIC Makers](https://ticmakers.com)

Demo
----

Flexbox Expo's snack

Install
-------

Install `@ticmakers-react-native/flexbox` package and save into `package.json`:

NPM

```shell
$ npm install @ticmakers-react-native/flexbox --save
```

Yarn

```shell
$ yarn add @ticmakers-react-native/flexbox
```

How to use?
-----------

```javascript
import React from 'react'
import { Col, Grid, Row } from '@ticmakers-react-native/flexbox'

export default class App extends React.Component {

  render() {
    return (
      <Grid>
        <Row size={3}>
          <Title>My example title</Title>
        </Row>

        <Row size={7}>
          <Col>
            <Image source={ require('./assets/image.jpg') } />
          </Col>
        </Row>

        <Row size={2}>
          <Col>
            <Text>Left</Text>
          </Col>

          <Col>
            <Text>Right</Text>
          </Col>
        </Row>
      </Grid>
    )
  }
}
```

Grid Properties
---------------

Name

Type

Default Value

Definition

size `(optional)`

\-

\-

\-

Row Properties
--------------

Name

Type

Default Value

Definition

size `(optional)`

\-

\-

\-

Col Properties
--------------

Name

Type

Default Value

Definition

size `(optional)`

\-

\-

\-

Todo
----

*   Test on iOS
*   Improve and add new features
*   Add more styles
*   Improve readme (example & demo)
*   Create tests

Version 1.0.1 ([Changelog](https://bitbucket.org/ticmakers/rn-core/src/master/CHANGELOG.md))
--------------------------------------------------------------------------------------------

## Index

### External modules

* ["Base"](modules/_base_.md)
* ["Col"](modules/_col_.md)
* ["Grid"](modules/_grid_.md)
* ["Row"](modules/_row_.md)
* ["index"](modules/_index_.md)

---

