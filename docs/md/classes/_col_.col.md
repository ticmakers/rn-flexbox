[@ticmakers-react-native/flexbox](../README.md) > ["Col"](../modules/_col_.md) > [Col](../classes/_col_.col.md)

# Class: Col

## Type parameters
#### SS 
## Hierarchy

↳  [Base](_base_.base.md)

**↳ Col**

## Index

### Constructors

* [constructor](_col_.col.md#constructor)

### Methods

* [UNSAFE_componentWillMount](_col_.col.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_col_.col.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_col_.col.md#unsafe_componentwillupdate)
* [ViewComponent](_col_.col.md#viewcomponent)
* [_deleteStyle](_col_.col.md#_deletestyle)
* [_filterStyle](_col_.col.md#_filterstyle)
* [_onPress](_col_.col.md#_onpress)
* [_processProps](_col_.col.md#_processprops)
* [componentDidCatch](_col_.col.md#componentdidcatch)
* [componentDidMount](_col_.col.md#componentdidmount)
* [componentDidUpdate](_col_.col.md#componentdidupdate)
* [componentWillMount](_col_.col.md#componentwillmount)
* [componentWillReceiveProps](_col_.col.md#componentwillreceiveprops)
* [componentWillUnmount](_col_.col.md#componentwillunmount)
* [componentWillUpdate](_col_.col.md#componentwillupdate)
* [getChildren](_col_.col.md#getchildren)
* [getSnapshotBeforeUpdate](_col_.col.md#getsnapshotbeforeupdate)
* [isPadding](_col_.col.md#ispadding)
* [isScroll](_col_.col.md#isscroll)
* [render](_col_.col.md#render)
* [shouldComponentUpdate](_col_.col.md#shouldcomponentupdate)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new Col**(props: *`IBaseProps`*): [Col](_col_.col.md)

*Inherited from [Base](_base_.base.md).[constructor](_base_.base.md#constructor)*

*Defined in Base.tsx:8*

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | `IBaseProps` |

**Returns:** [Col](_col_.col.md)

___

## Methods

<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:638*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<`IBaseProps`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:670*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IBaseProps`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<`IBaseProps`>*, nextState: *`Readonly`<`IBaseStates`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:698*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IBaseProps`> |
| nextState | `Readonly`<`IBaseStates`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="viewcomponent"></a>

###  ViewComponent

▸ **ViewComponent**(): `Element`

*Overrides [Base](_base_.base.md).[ViewComponent](_base_.base.md#viewcomponent)*

*Defined in Col.tsx:12*

**Returns:** `Element`

___
<a id="_deletestyle"></a>

###  _deleteStyle

▸ **_deleteStyle**(style: *`TypeStyle`*, prop: *`string`*): `any`

*Inherited from [Base](_base_.base.md).[_deleteStyle](_base_.base.md#_deletestyle)*

*Defined in Base.tsx:124*

**Parameters:**

| Name | Type |
| ------ | ------ |
| style | `TypeStyle` |
| prop | `string` |

**Returns:** `any`

___
<a id="_filterstyle"></a>

###  _filterStyle

▸ **_filterStyle**(prop: *`string`*): `any`

*Inherited from [Base](_base_.base.md).[_filterStyle](_base_.base.md#_filterstyle)*

*Defined in Base.tsx:107*

**Parameters:**

| Name | Type |
| ------ | ------ |
| prop | `string` |

**Returns:** `any`

___
<a id="_onpress"></a>

###  _onPress

▸ **_onPress**(): `any`

*Inherited from [Base](_base_.base.md).[_onPress](_base_.base.md#_onpress)*

*Defined in Base.tsx:81*

**Returns:** `any`

___
<a id="_processprops"></a>

###  _processProps

▸ **_processProps**(): `IColStates`

*Overrides [Base](_base_.base.md).[_processProps](_base_.base.md#_processprops)*

*Defined in Col.tsx:53*

**Returns:** `IColStates`

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:567*

Catches exceptions generated in descendant components. Unhandled exceptions will cause the entire component tree to unmount.

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:546*

Called immediately after a component is mounted. Setting state here will trigger re-rendering.

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<`IBaseProps`>*, prevState: *`Readonly`<`IBaseStates`>*, snapshot?: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:609*

Called immediately after updating occurs. Not called for the initial render.

The snapshot is only present if getSnapshotBeforeUpdate is present and returns non-null.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<`IBaseProps`> |
| prevState | `Readonly`<`IBaseStates`> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:624*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<`IBaseProps`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:653*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IBaseProps`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:562*

Called immediately before a component is destroyed. Perform any necessary cleanup in this method, such as cancelled network requests, or cleaning up any DOM elements created in `componentDidMount`.

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<`IBaseProps`>*, nextState: *`Readonly`<`IBaseStates`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:683*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IBaseProps`> |
| nextState | `Readonly`<`IBaseStates`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="getchildren"></a>

###  getChildren

▸ **getChildren**(): `undefined` \| `null` \| `string` \| `number` \| `false` \| `true` \| `__type` \| `ReactElement`<`any`, `string` \| `function` \| `object`> \| `ReactNodeArray` \| `ReactPortal`

*Inherited from [Base](_base_.base.md).[getChildren](_base_.base.md#getchildren)*

*Defined in Base.tsx:77*

**Returns:** `undefined` \| `null` \| `string` \| `number` \| `false` \| `true` \| `__type` \| `ReactElement`<`any`, `string` \| `function` \| `object`> \| `ReactNodeArray` \| `ReactPortal`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<`IBaseProps`>*, prevState: *`Readonly`<`IBaseStates`>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:603*

Runs before React applies the result of `render` to the document, and returns an object to be given to componentDidUpdate. Useful for saving things such as scroll position before `render` causes changes to it.

Note: the presence of getSnapshotBeforeUpdate prevents any of the deprecated lifecycle events from running.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<`IBaseProps`> |
| prevState | `Readonly`<`IBaseStates`> |

**Returns:** `SS` \| `null`

___
<a id="ispadding"></a>

###  isPadding

▸ **isPadding**(): `undefined` \| `false` \| `true`

*Inherited from [Base](_base_.base.md).[isPadding](_base_.base.md#ispadding)*

*Defined in Base.tsx:67*

**Returns:** `undefined` \| `false` \| `true`

___
<a id="isscroll"></a>

###  isScroll

▸ **isScroll**(): `undefined` \| `false` \| `true`

*Inherited from [Base](_base_.base.md).[isScroll](_base_.base.md#isscroll)*

*Defined in Base.tsx:72*

**Returns:** `undefined` \| `false` \| `true`

___
<a id="render"></a>

###  render

▸ **render**(): `Element`

*Inherited from [Base](_base_.base.md).[render](_base_.base.md#render)*

*Defined in Base.tsx:14*

**Returns:** `Element`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<`IBaseProps`>*, nextState: *`Readonly`<`IBaseStates`>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Flexbox/node_modules/@types/react/index.d.ts:557*

Called to determine whether the change in props and state should trigger a re-render.

`Component` always returns true. `PureComponent` implements a shallow comparison on props and state and returns true if any props or states have changed.

If false is returned, `Component#render`, `componentWillUpdate` and `componentDidUpdate` will not be called.

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IBaseProps`> |
| nextState | `Readonly`<`IBaseStates`> |
| nextContext | `any` |

**Returns:** `boolean`

___

